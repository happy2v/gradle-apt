package happy2v.gradle.apt;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.mysema.query.types.OrderSpecifier;

@SpringBootApplication
public class AptApplication implements CommandLineRunner {

	private final Logger logger = LogManager.getLogger(AptApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(AptApplication.class);
	}

	@Override
	public void run(String... args) throws Exception {
		Person person = new Person();
		// lombok compile 이후 사용 // delombok 실행시 생성
		person.setAge(30);
		logger.info("hello {}" + person);
		
		// QueryDSL compile 이후 사용 // build 시 생성
		OrderSpecifier<String> asc = QPerson.person.name.asc();
		logger.info("person name asc {}", asc);
	}
}
